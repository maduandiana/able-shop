const User = require('../models/users')
const bcrypt = require('bcryptjs')
const jwt = require('jsonwebtoken')
const keys = require('../config/keys')

async function generatePassword(password,salt){
    return bcrypt.hash(password,salt)
}

async function makeJWT(user){
    const payload = {
        id: user.id,
        email: user.email,
        role: user.role,
        username: user.username
    }
    return jwt.sign(
        payload,
        keys.secretOrKey, {expiresIn: 3600}
    );
}

async function createUser(user){
    const {email, username, password} = user
    const salt = await bcrypt.genSalt(10)
    const hashedPassword = await generatePassword(password,salt)
    const newUser = new User({
        username:username,
        email:email,
        password:hashedPassword
    })
    newUser.save()
    const token = await makeJWT(newUser)
    return token
}

exports.signUp = async function(req,res){
    try{
        const found = await User.findOne({email: req.body.email})
        if(!found){
            const token = await createUser(req.body)
            return res.status(200).json({
                auth: true,
                token: token
            })
        }
        return res.status(400).json({email:'User already exist!'})
    }
    catch(e){
        return res.status(400).json(e.message)
    }
}


exports.login = async function(req,res){
    try{
    let errors = {}
    const {email, password} = req.body
    User.findOne({email}).then(async(user)=>{
        if(!user){
            errors.email = 'Пользователь с таким email не найдено!'
            return res.status(400).json(errors)
        }
        const isMatch = bcrypt.compareSync(password,user.password)
        if(!isMatch){
            errors.password = 'Неправильный пароль!'
            return res.status(400).json(errors)
        }
        const token = await makeJWT(user)
        return res.status(200).json({
            auth: true,
            token: token
        })
    })}
    catch(e){
        return res.status(400).json(e.message)
    }
}