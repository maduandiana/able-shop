const Basket = require('../models/basket')

addToBasket = (req, res) => {
    const body = req.body
    const {name,price,image} = req.body.product
    if (!body) { return res.status(400).json({ success: false, error: 'You must provide a product', }) }
    const product = new Basket({
        user:body.user,
        size:body.size,
        product: { name: req.body.product.name, price: req.body.product.price,image :req.body.product.image}
      });
    if (!product) { return res.status(400).json({ success: false, error: err }) }
    product.save().then(() => {
        return res.status(201).json({
            success: true,
            id: product._id,
            message: 'Product created!',
        })
        })
    .catch(error => { return res.status(400).json({ error, message: 'Basket not created!', })
    })
}


getBaskets = async (req, res) => {
    await Basket.find({}, (err, products) => {
    if (err) {
    return res.status(400).json({ success: false, error: err })
    }
    if (!products.length) {
    return res
    .status(404)
    .json({ success: false, error: `Products in basket not found` })
    }
   
    return res.status(200).json({ success: true, data: products })
    }).catch(err => console.log(err))
   }



   deleteFromProduct = async (req, res) => {
    await Basket.findOneAndDelete({ _id: req.params.id }, (err, product) => {
    if (err) {
    return res.status(400).json({ success: false, error: err })
    }
    if (!product) {
    return res
    .status(404)
    .json({ success: false, error: `Product not found` })
    }
    return res.status(200).json({ success: true, data: product })
    }).catch(err => console.log(err))
   }
module.exports = {
    addToBasket,
    deleteFromProduct,
    getBaskets,
}