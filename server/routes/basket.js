const express = require('express')
const basketController = require('../controllers/basketController')
const router = express.Router()

router.get('/' , basketController.getBaskets)
router.post('/add' , basketController.addToBasket)
router.delete('/:id' , basketController.deleteFromProduct)

module.exports = router