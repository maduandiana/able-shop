const express = require('express')
const productCotroller = require('../controllers/productController')
const router = express.Router()

router.get('/' , productCotroller.getProducts)
router.post('/add' , productCotroller.createProduct)
router.put('/:id' , productCotroller.updateProduct)
router.delete('/:id' , productCotroller.deleteProduct)
router.get('/:id' , productCotroller.getProductById)

module.exports = router