const express = require('express')
const userController = require('../controllers/userController')
const profileController = require('../controllers/profileController')
const router = express.Router()
const passport = require('passport')

router.post('/signup' , userController.signUp)
router.post('/login' , userController.login)
router.get('/profile' ,passport.authenticate('jwt', { session: false }), profileController.getProfile)

module.exports = router