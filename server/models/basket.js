const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Basket = new Schema(
 {
    size: { type: String, required: true },
    product : {
        name: {type:String,required: true},
        price: {type:Number, required:true},
        image:{type: String, required:true}
    },
    user : {
        type:Schema.ObjectId,
        ref:'User',
        required: true
    }
 },
 { timestamps: true },
)
module.exports = mongoose.model('Basket', Basket)