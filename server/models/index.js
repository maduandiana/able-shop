const mongoose = require('mongoose')
const Schema = mongoose.Schema
const Products = new Schema(
 {
    name: { type: String, required: true },
    description: { type: String, required: true },
    details: { type: String, required: true },
    type: { type: String, required: true },
    arrival: { type: Boolean, required: true },
    bestseller: { type: Boolean, required: true },
    price: { type: Number, required: true },
    image: { type: String, required: true },
    image2: { type: String, required: true },
    image3: { type: String, required: true },
 },
 { timestamps: true },
)
module.exports = mongoose.model('Products', Products)