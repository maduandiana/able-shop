const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const db = require('./db/index')
const app = express()
const productRouter = require('./routes/product')
const basketRouter = require('./routes/basket')
const authRouter = require('./routes/users')
const passport = require('passport');
app.use(passport.initialize());
app.use(cors())
app.use(bodyParser.urlencoded({extended:true}))
app.use(bodyParser.json())

app.get('/',(req,res)=>{
    console.log('Hello world!')
})
require('./config/passport')(passport);
app.use('/product' ,productRouter)
app.use('/basket' ,basketRouter)
app.use('/auth' ,authRouter)

db.on('error', console.error.bind(console, 'MongoDB connection error:'))

app.listen(3000,() => {
    console.log('App running in 3000 port!')
})