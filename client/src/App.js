import React from 'react';
import './App.css';
import Main from './components/main'
import {BrowserRouter,Route} from 'react-router-dom'
import Page from './components/collections/index'
import SignUp from './components/auth/signup';
import Login from './components/auth/login';
import ProductPage from './components/productDetails';
import store from './store'
import {Provider} from 'react-redux';
import setAuthToken from "./utils/setToken";
import jwt_decode from "jwt-decode"
import {logOut, setCurrentUser} from "./actions/authActions";
import {PrivateRoute} from './private-route'
import AdminLayout from './admin/adminLogin'
if (localStorage.jwtToken) {
  setAuthToken(localStorage.jwtToken);
  const decoded = jwt_decode(localStorage.jwtToken);
  store.dispatch(setCurrentUser(decoded));
  const currentTime = Date.now()/1000;
  console.log("decoded.exp: " + decoded.exp)
  console.log("currentTime: " + currentTime)
  if (decoded.exp<currentTime) {
      store.dispatch(logOut());
      window.location.href = '/';
  }
}
function App() {

  return (
    <Provider store={store}>
      <BrowserRouter >
      <div className="App">
        <Route exact path='/collections/:type' component={Page}/>
        <Route exact path='/' component={Main}/>
        <Route exact path='/signup' component={SignUp} />
        <Route exact path='/login' component={Login} />
        <Route exact path='/details/:pid' component={ProductPage} />
        <Route exact path='/collections/details/:pid' component={ProductPage} />
        <PrivateRoute exact path='/admin' component={AdminLayout} />
        <PrivateRoute exact path='/admin/products' component={AdminLayout} />
        <PrivateRoute exact path='/admin/users' component={AdminLayout} />
        </div>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
