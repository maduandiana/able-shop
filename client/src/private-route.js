import React from 'react'
import {Route,Redirect} from 'react-router-dom'

export const PrivateRoute = ({component: Component, ...rest}) =>(
    <Route {...rest} render = {props => (
        // eslint-disable-next-line 
        localStorage.getItem('role') == 'admin' && localStorage.getItem('jwtToken')  ?
        <Component {...props} />:
        <Redirect to={{pathname:'/', state:{from:props.location}, propsSearch:'У вас нету доступа!'}} />
    )}
    />
)