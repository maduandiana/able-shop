import React, {useEffect, useState} from 'react'
import connect from "react-redux/es/connect/connect";
import { Table, Space,Button,Modal,Form, Input, Select,Switch } from 'antd';
import {getProducts,deleteProductById,saveProduct} from '../actions/productActions'
const { Column } = Table;
const { Option } = Select;

function ProductAdmin(props){  
    useEffect(() => {
        props.getProducts()
        // eslint-disable-next-line 
    },[]);
    const [form] = Form.useForm();
    const onReset = () => {
        form.resetFields();
      };
    const [visible,setVisible] = useState(false)
    let showModal = () => {
        setVisible(true)
      };
    let closeModal = () => {
        setVisible(false)
      };
    const {products} = props.productReducer
    const layout = {
        labelCol: { span: 8 },
        wrapperCol: { span: 16 },
      };
      const tailLayout = {
        wrapperCol: { offset: 8, span: 16 },
      };
    let addProduct = (values) =>{
        props.saveProduct(values,closeModal,onReset)
    }
    return(
        <div className='component'>
            <h1>Продукты</h1>
            <Button type="primary" onClick={showModal}>
                Добавить новый продукт
            </Button>
            <Modal
            visible={visible}
            title="Добавить новый продукт"
            onCancel={closeModal}
            footer={''}
            
            >
          <Form {...layout} form={form} name="control-hooks" onFinish={addProduct}>
      <Form.Item name="name" label="Продукт" rules={[{ required: true }]}>
        <Input placeholder='Название продукта'/>
      </Form.Item>
      <Form.Item name="description" label="Описание" rules={[{ required: true }]}>
        <Input placeholder='Описание продукта'/>
      </Form.Item>
      <Form.Item name="details" label="Описание" rules={[{ required: true }]}>
        <Input placeholder='Описание продукта'/>
      </Form.Item>
      <Form.Item name="type" label="Тип продукта" rules={[{ required: true }]}>
        <Select
          placeholder="Выберите тип продукта"
          allowClear
        >
          <Option value="men">men</Option>
          <Option value="women">women</Option>
          <Option value="goods">goods</Option>
        </Select>
      </Form.Item>
      <Form.Item name='arrival' label="Arrival">
          <Switch />
        </Form.Item>
        <Form.Item name='bestseller' label="Bestseller">
          <Switch />
        </Form.Item>
        <Form.Item name="price" label="Цена" rules={[{ required: true }]}>
            <Input placeholder='Цена продукта' />
        </Form.Item>
        <Form.Item name="image" label="Рисунок" rules={[{ required: true }]}>
            <Input placeholder='Рисунок продукта'/>
        </Form.Item>
        <Form.Item name="image2" label="Рисунок" rules={[{ required: true }]}>
            <Input placeholder='Рисунок продукта'/>
        </Form.Item>
        <Form.Item name="image3" label="Цена" rules={[{ required: true }]}>
            <Input placeholder='Рисунок продукта'/>
        </Form.Item>
      <Form.Item {...tailLayout}>
        <Button type="primary" htmlType="submit" >
          Добавить
        </Button>
        <Button htmlType="button" onClick={onReset}>
          Сбросить
        </Button>
      </Form.Item>
    </Form>
        </Modal>

            <Table dataSource={products.data} pagination={{pageSize:5}}>
                <Column title="Имя продукта" dataIndex="name" key="name" />
                <Column title="Описание" dataIndex="description" key="description" />
                <Column title="Цена" dataIndex="price" key="price" />
                <Column title="Тип" dataIndex="type" key="type" />
                <Column title="Рисунок" dataIndex="image" key="image" render={image => (
            <img src={image} className='admin-img' alt='image423432'/>
          )}> </Column>
          <Column title="Рисунок 2" dataIndex="image2" key="image2" render={image => (
            <img src={image} className='admin-img' alt='image343'/>
          )}> </Column>
                <Column
                title="Action"
                key="action"
                render={(text, record) => (
                    <Space size="middle">
                    <Button onClick={() => props.deleteProductById(record._id)}>Delete</Button>
                    </Space>
                )}
                />
            </Table>
        </div>
    )
}

const mapStateToProps = (state) => ({
    productReducer:state.productReducer
});

export default connect(mapStateToProps, {getProducts,deleteProductById,saveProduct})(ProductAdmin);
