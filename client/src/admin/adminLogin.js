import React from 'react';
import { Layout, Menu } from 'antd';
import {Route} from 'react-router-dom'
import {
  ShopOutlined,
  UserOutlined,
} from '@ant-design/icons';
import {Link} from 'react-router-dom'
import {login} from '../actions/authActions'
import connect from "react-redux/es/connect/connect";
import ProductAdmin from './productComponent'
import UserAdmin from './userComponent'
const {  Content, Sider } = Layout;

function AdminLayout(props){
let openComponent = (path) =>{
  props.history.push(path)
}
    return(
        <section className='admin-section'>
            <Layout>
    <Sider
      style={{
        overflow: 'auto',
        height: '100vh',
        position: 'fixed',
        left: 0,
      }}
    >
    <div id="logo">
        <Link to='/'>
            <img src='/images/logo.png' alt='logo' className='logo' />
        </Link>
     </div>
      <Menu theme="dark" mode="inline" defaultSelectedKeys={['4']}>
        <Menu.Item key="1" icon={<ShopOutlined/>} onClick={() => openComponent('/admin/products')}>
          Продукты
        </Menu.Item>
        <Menu.Item key="2" icon={<UserOutlined />} onClick={() =>openComponent('/admin/users')}>
          Пользователи
        </Menu.Item>
      </Menu>
    </Sider>
    <Layout className="site-layout" style={{ marginLeft: 200 }}>
      <Content style={{ margin: '0 16px 0', overflow: 'initial' }}>
        <div className="site-layout-background" style={{ padding: 24, textAlign: 'center' }}>
          <Route exact path={'/admin/products'} component={ProductAdmin} />
          <Route exact path={'/admin/users'} component={UserAdmin} />
        </div>
      </Content>
    </Layout>
  </Layout>,
        </section>
    )
}
const mapStateToProps = (state) => ({
    
});

export default connect(mapStateToProps, {login})(AdminLayout);
