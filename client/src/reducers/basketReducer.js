import {GET_BASKET} from '../actions/types'

const initialState = {
    basket:[]
}

export default function (state=initialState,action){
    switch(action.type){
        case GET_BASKET:
            return{
                ...state,
                basket: action.payload.data
            }
        default:
            return state;
    }
}