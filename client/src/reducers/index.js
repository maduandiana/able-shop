import {combineReducers} from 'redux'
import productReducer from './productReducer'
import errorReducer from './errorReducer'
import userReducer from './userReducer'
import basketReducer from './basketReducer'
export default combineReducers({
    productReducer: productReducer,
    errorReducer: errorReducer,
    userReducer: userReducer,
    basketReducer:basketReducer
})