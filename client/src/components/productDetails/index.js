import React,{useState, useEffect} from 'react';
import Header from '../header/index.js'
import Footer from '../footer/index.js'
import { HeartOutlined ,ShoppingCartOutlined} from '@ant-design/icons';
import {getProductById} from '../../actions/productActions'
import {addToBasket, getBasket} from '../../actions/basketActions'
import { Modal, Button } from 'antd';
import {Link} from 'react-router-dom'
import connect from "react-redux/es/connect/connect";

function ProductPage(props){
    const [size,setSize] = useState('XS')
    const [visible,setVisible] = useState(false)
    const [visible2, setVisible2] = useState(false)
    const [xs,setXS] = useState(91)
    const [s,setS] = useState(97)
    const [m,setM] = useState(102)
    const [l,setL] = useState(107)
    const [xl,setXL] = useState(112)
    
    useEffect(() => {
        props.getProductById(props.match.params.pid)
        window.scrollTo(0,0)
        // eslint-disable-next-line 
    },[]);
    
    let changeCM = () =>{
        setXS(91)
        setXL(112)
        setM(102)
        setL(107)
        setS(97)
        document.getElementById('btn-1').style.color = 'white'
        document.getElementById('btn-1').style.backgroundColor = '#2c241d'
        document.getElementById('btn-2').style.color = 'black'
        document.getElementById('btn-2').style.backgroundColor = 'white'
    }
    let changeIN = () =>{
        setXS(36)
        setXL(44)
        setM(40)
        setL(42)
        setS(38)
        document.getElementById('btn-2').style.color = 'white'
        document.getElementById('btn-2').style.backgroundColor = '#2c241d'
        document.getElementById('btn-1').style.color = 'black'
        document.getElementById('btn-1').style.backgroundColor = 'white'
    }
    let showModal = () => {
        setVisible(true);
      };
    
    let handleCancel = () => {
        setVisible(false);
      };
    let showModal2 = () => {
        setVisible2(true);
    };
    let handleCancel2 = () => {
        setVisible2(false);
    };  
    let addToBasket = (name,image,price) =>{
        let data = {
            size: size,
            product: {
                name:name,
                price:price,
                image:image
            },
            user: props.userReducer.userProfile.data._id
        }
        props.addToBasket(data,props.getBasket())
    }
    const {product} = props.productReducer
    const {isAuthenticated} = props.userReducer
    return(
        <section>
            <Header/>
            <div className='details'> 
                <div className='details-part-1'>
                    <img src={product && product.data ? product.data.image : null} alt='img1'></img>
                    <img src={product && product.data ? product.data.image2 : null} alt='img1'></img>
                    <img src={product && product.data ? product.data.image3 : null} alt='img1'></img>
                </div>
                <div className='details-part-2'>
                    <img src={product && product.data ? product.data.image : null} alt='img1'></img>
                    <img src={product && product.data ? product.data.image2 : null} alt='img1'></img>
                    <img src={product && product.data ? product.data.image3 : null} alt='img1'></img>
                </div>
                <div className='details-part-3'>
                    <h2>
                    {product && product.data ? product.data.name : 'Product name'}
                    </h2>
                    <p>{product && product.data ? product.data.price : '0'}</p>
                    <p>Make 3 payments of £10.00. Klarna. No fees.<span onClick = {() => {showModal2()}} className='more'>Learn more</span></p>
                    <p>Size:{size}</p>
                    <div className='size-div'>
                        <span onClick={()=>{setSize('XS')}}  className='size-span'>XS</span>
                        <span onClick={()=>{setSize('S')}}   className='size-span'>S</span>
                        <span onClick={()=>{setSize('M')}}   className='size-span'>M</span>
                        <span onClick={()=>{setSize('L')}}  className='size-span'>L</span>
                        <span onClick={()=>{setSize('XL')}}  className='size-span'>XL</span>
                        <span onClick={()=>{setSize('XLL')}}  className='size-span'>XLL</span>
                    </div>
                    <p onClick={()=>{showModal()}} className='more'>Find your perfect fit</p>
                    <div className='buttons'>
                        {isAuthenticated ? 
                        <button className='btn-1' onClick={() => addToBasket(product && product.data ? product.data.name : 'Product name',product && product.data ? product.data.image : 'image',product && product.data ? product.data.price : 0)}>Add <ShoppingCartOutlined/></button>:
                        <Link to='/login'><button className='btn-1'>Add <ShoppingCartOutlined/></button></Link>
}

                        <button className='btn-2'><HeartOutlined/></button>
                    </div>
                    <div className='benefits'>
                        <div className='benefits-item'>
                            <img src='https://cdn.shopify.com/s/files/1/0235/2617/files/sale-icon_180x.png?v=1595516820' alt='icon' />
                            <p>15% Off First Order</p>
                        </div>
                        <div className='benefits-item'>
                            <img src='https://cdn.shopify.com/s/files/1/0235/2617/files/fast_db92c728-18b3-4adb-8edb-89df43c4d565_180x.png?v=1596095767' alt='icon' />
                            <p>Extended 60 Day Returns</p>
                        </div>
                        <div className='benefits-item'>
                            <img src='https://cdn.shopify.com/s/files/1/0235/2617/files/globe_1293d8fc-d66a-4abc-9eab-9f5e624473de_180x.png?v=1595516869' alt='icon' />
                            <p>Worldwide Shipping</p>
                        </div>
                    </div>
                    <details className='product-details'>
                        <summary>Description</summary>
                        {product && product.data ? product.data.description : 'Product description'}
                    </details>
                    <details className='product-details'>
                        <summary>Details</summary>
                        {product && product.data ? product.data.details : 'Product details'}
                    </details>
                    <details className='product-details'>
                        <summary>Delivery</summary>
                        <ul>
                            <li>Free worldwide shipping on all orders over £50</li>
                            <li>• Orders dispatched within 1-2 working days</li>
                            <li>Please note due to COVID-19, we may be experiencing some delays in processing orders.</li>
                        </ul>
                    </details>
                </div>
            </div>
            <Modal
          visible={visible}
          title="Size Guide"
          onCancel={handleCancel}
          width={600}
          className='modal'
          footer={[
            <Button key="back" onClick={handleCancel}>
            </Button>]}
            >
            <div className='modal'>
                <h2>Mens Tops</h2>
                <p>Measure under your arms, around the fullest part of your Chest</p>
                <hr/>
                <table className='size-table'>
                    <tr><th>SIZE</th><th className='t2'>CHEST</th></tr>
                    <tr><td>XS</td><td className='t2'>{xs}</td></tr>
                    <tr><td>S</td><td className='t2'>{s}</td></tr>
                    <tr><td>M</td><td className='t2'>{m}</td></tr>
                    <tr><td>L</td><td className='t2'>{l}</td></tr>
                    <tr><td>XL</td><td className='t2'>{xl}</td></tr>
                </table>
                <div className='size-btn'>
                    <button className='size-btn-1' id='btn-1' onClick={()=>{changeCM()}}>CM</button>
                    <button className='size-btn-2' id='btn-2' onClick={()=>{changeIN()}}>IN</button>
                </div>
                <hr/>
                <p>We are always here to answer any questions regarding our products or your purchase.Still got questions? Check out our Help Centre.</p>
            </div>
         
            </Modal>
            <Modal
                visible={visible2}
                onCancel = {handleCancel2}
                width={600}
                className='modal2'
                footer={[<Button key="back" onClick={handleCancel2}></Button>]}>
                
                    <div className = 'modal2'>
                        <h2> Pay 3 instalments of £10.00 </h2> 
                        <p> Pay in 3 interest - free instalments so you can spread the cost. </p> <br/>
                        <ul>
                            <li> Add item(s) to your cart  </li>
                            <li> Go to checkout and choose <span className = 'klarna'> <b> Klarna </b></span> </li>
                            <li> Enter your debit or credit card information </li>
                            <li> Pay later in 3 instalments.The first payment is taken when the order is processed and the remaining 2 are automatically taken every 30 days. </li>
                            
                        </ul>
                    </div>
                    <div>
                        <button onClick = {handleCancel2} className='btn-modal2'> Close </button> 
                    </div>
            </Modal>
            <Footer/>
        </section>
    )
}

const mapStateToProps = (state) => ({
    productReducer: state.productReducer,
    userReducer: state.userReducer
});

export default connect(mapStateToProps, {getProductById,addToBasket,getBasket})(ProductPage);
