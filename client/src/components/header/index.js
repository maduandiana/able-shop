import React, {Component} from 'react';
import { Menu, Dropdown,Drawer} from 'antd';
import 'antd/dist/antd.css';
import {Link} from 'react-router-dom'
import {SearchOutlined,UserOutlined,HeartOutlined,ShoppingCartOutlined,CloseOutlined} from '@ant-design/icons'
import BasketItem from '../basket';
import BasketTotal from '../basket/total';
import {getProfile} from '../../actions/authActions'
import connect from "react-redux/es/connect/connect";
import {logOut} from '../../actions/authActions'
import {getBasket } from '../../actions/basketActions'
class Header extends Component{
    state={
        visible:false,
        visible2:false,
        placement: 'right',
    }
    componentDidMount(){
        if(this.props.userReducer.isAuthenticated){
            this.props.getProfile()
            this.props.getBasket()
        }
    }
   
    showBox = ()=>{
        this.setState({
            visible:true
        })
    }
    closeBox = ()=>{
        this.setState({
            visible:false
        })
    }
    showCardBox = ()=>{
        this.setState({
            visible2:true
        })
    }
    closeCardBox = ()=>{
        this.setState({
            visible2:false
        })
    }

    render(){
        const logout = (
            <Menu className='menu-bar'>
              <Menu.Item key="1" onClick={()=> this.props.logOut()}>Выйти</Menu.Item>
            </Menu>
        )
        const menu = (
            <Menu className='menu-bar'>
              <Menu.Item key="1">All arrivals</Menu.Item>
              <Menu.Item key="2">T-shirts</Menu.Item>
              <Menu.Item key="3">Bottoms</Menu.Item>
              <Menu.Item key="4">Headwears</Menu.Item>
              <Menu.Item key="3">Coods</Menu.Item>
              <Menu.Item key="3">All mens</Menu.Item>
            </Menu>
          );
          const menu1 = (
            <Menu className='menu-bar'>
              <Menu.Item key="1">All arrivals</Menu.Item>
              <Menu.Item key="2">T-shirts</Menu.Item>
              <Menu.Item key="3">Bottoms</Menu.Item>
              <Menu.Item key="4">Overals</Menu.Item>
              <Menu.Item key="3">Coods</Menu.Item>
              <Menu.Item key="3">All womens</Menu.Item>
            </Menu>
          );
          const menu2 = (
            <Menu className='menu-bar'>
              <Menu.Item key="1">All arrivals</Menu.Item>
              <Menu.Item key="2">T-shirts</Menu.Item>
              <Menu.Item key="3">Socks</Menu.Item>
              <Menu.Item key="4">Leather coods</Menu.Item>
              <Menu.Item key="3">Coods</Menu.Item>
              <Menu.Item key="3">All coods</Menu.Item>
            </Menu>
          );
          const searchBox=(
              <div className='searchBox'>
                  <div className='row2'>
                    <p>What are you looking for?</p>
                    <CloseOutlined onClick={()=>this.closeBox()} />
                  </div>
                  
                  <div className='row2'>
                    <input type='text' className='searsh-input' placeholder='Searsh Products' />
                    <SearchOutlined className='search-icon'/>
                  </div> 
              </div>
          )
          const { placement, visible2 } = this.state;
          const {basket} = this.props.basketReducer
         const {userProfile,isAuthenticated} = this.props.userReducer
        return(
            <div className='header'>
                <div className='row'>
                    <Link to='/'>
                        <img src='/images/logo.png' alt='logo' className='logo' />
                    </Link>
                   
                    <div>
                        <Dropdown overlay={menu}>
                            <a className="a-link" href='www.google.com' onClick={e => e.preventDefault()}>Men</a>
                        </Dropdown>
                        <Dropdown overlay={menu1}>
                            <a className="a-link" href='www.google.com' onClick={e => e.preventDefault()}>Women</a>
                        </Dropdown>
                        <Dropdown overlay={menu2}>
                            <a className="a-link" href='www.google.com' onClick={e => e.preventDefault()}>Coods</a>
                        </Dropdown>
                        <a className="a-link" href='www.google.com' onClick={e => e.preventDefault()}>Journal</a>
                    </div>
                </div>
               
                <div className='icon-links'>
                    <SearchOutlined onClick={()=>this.showBox()}/>
                    <HeartOutlined />
                    <ShoppingCartOutlined onClick={()=>this.showCardBox()}/>
                    <p className='card-icon'>Card(0)</p>
                    {isAuthenticated && userProfile && userProfile.data ?
                    <Dropdown overlay={logout} ><p id='user-name'>{userProfile.data.username}</p></Dropdown>:
                    <Link to='signup'>
                        <UserOutlined />
                    </Link>
                    }
                </div>
                {this.state.visible ? 
                searchBox : null}
                 <Drawer
                    title="Card"
                    placement={placement}
                    closable={false}
                    onClose={this.closeCardBox}
                    visible={visible2}
                    key={placement}
                    className='card-box'
                    width='500'
                    >
                    {basket ? 
                    <div>
                        {basket.map(item => (
                            <BasketItem id={item._id} name={item.product.name} price={item.product.price} size={item.size} image={item.product.image}/>
                        ))}
                       
                    </div>:
                    <p>Нет продуктов</p>}
                    
                    {basket ?  <BasketTotal/>: null}
                </Drawer>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userReducer:state.userReducer,
    basketReducer: state.basketReducer
});

export default connect(mapStateToProps, {getProfile,logOut,getBasket})(Header);
