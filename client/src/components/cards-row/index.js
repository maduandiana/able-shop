import React,{Component} from 'react'
import Card from '../card/index'
import {getProducts} from '../../actions/productActions'
import connect from "react-redux/es/connect/connect";
class CardsRow extends Component{
   
    componentDidMount(){
        this.props.getProducts()
    }
    render(){
        // eslint-disable-next-line
       let cardList
        return(
            <section className='cards-section'>
                <h2>{this.props.name}</h2>
                <div className='row'>
    {
                     this.props.productReducer.products && this.props.productReducer.products.data ?
            cardList = this.props.productReducer.products.data.map((item) =>(
                item.arrival && this.props.flag === 'arrival' ?
                <Card key={item._id} id={item._id} name={item.name} description={item.description} details={item.details} price={item.price} type={item.type} image={item.image}
                            image2={item.image2} image3={item.image3}/> :
                            item.bestseller && this.props.flag === 'bestseller' ?
                            <Card key={item.id} name={item.name} description={item.description} details={item.details} price={item.price} type={item.type} image={item.image}
                            image2={item.image2} image3={item.image3}/> :
                            item.arrival && item.bestseller ?
                            <Card key={item.id} name={item.name} description={item.description} details={item.details} price={item.price} type={item.type} image={item.image}
                            image2={item.image2} image3={item.image3}/> :
                            null
                             )): null 
            }

         </div>
            </section>
        )
    }
}
const mapStateToProps = (state) => ({
    productReducer: state.productReducer
});

export default connect(mapStateToProps, {getProducts})(CardsRow);
