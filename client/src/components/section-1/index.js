import React, {Component} from 'react'
import {Link} from 'react-router-dom'
class Section1 extends Component{
    render(){
        return(
            <div className='section-1'>
            <div className='div-center'>
                <h1>Acid Wash T-Shirts</h1>
                <div>
                    <Link to='/collections/men'>
                        <button className='shop-btn'>Shop Men</button>
                    </Link>
                    <Link to='/collections/women'>
                        <button className='shop-btn'>Shop Women</button>
                    </Link>
                    
                </div>
            </div>
            </div>
        )
    }
}

export default Section1;