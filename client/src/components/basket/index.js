import React, {Component} from 'react'
import {deleteBasket} from '../../actions/basketActions'
import connect from "react-redux/es/connect/connect";
class BasketItem extends Component{
    render(){
       
        return(
           <div className='basket-item'>
               <div className='basket-div-1'>
                   <img src={this.props.image}  alt='item'/>
               </div>
               <div className='basket-div-2'>
                    <p>{this.props.name}</p>
                    <p>Size: {this.props.size}</p>
                    <p>Colour: Olive</p>
                    <select>
                       <option>1</option>
                       <option>2</option>
                       <option>3</option>
                       <option>4</option>
                       <option>5</option>
                    </select>
               </div>
               <div className='basket-div-3'>
                   <p>{this.props.price}</p>
                   <p onClick={()=> this.props.deleteBasket(this.props.id)}>Remove</p>
               </div>
           </div>
        )
    }
}

const mapStateToProps = (state) => ({
    userReducer:state.userReducer,
    basketReducer: state.basketReducer
});

export default connect(mapStateToProps, {deleteBasket})(BasketItem);
