import React, {Component} from 'react'
import connect from "react-redux/es/connect/connect";
class BasketTotal extends Component{
    render(){
        const {basket} = this.props.basketReducer
        let total = 0
        basket.map(item => (
            total = total + item.product.price
        ))
        return(
           <div className='basket-total'>
               <hr></hr>
               <div>
                   <p>Order Value</p>
                   <p>£{total}</p>
               </div>
               <div>
                   <p>Delivery</p>
                   <p>£3.00</p>
               </div>
               <div>
                   <h5>TOTAL</h5>
                   <h5>£{total + 3}</h5>
               </div>
               <button className='checkout-btn'>Checkout</button>
               <button className='continue-btn'>Continue shopping</button>
           </div>
        )
    }
}

const mapStateToProps = (state) => ({
    basketReducer: state.basketReducer
});

export default connect(mapStateToProps, {})(BasketTotal);
