import React, {Component} from 'react'
import Header from './header/index'
import Section1 from './section-1/index'
import Section2 from './collections/page'
import Benefits from './benefits/index'
import CardsRow from './cards-row/index'
import Section3 from './section-3/index'
import Section4 from './benefits/section-4'
import Footer from './footer/index'

class Main extends Component{
   
    render(){
       
        return(
            <div>
                <Header history={this.props.history}/>
                <Section1/>
                <Benefits/>
                <Section2 link1='Men' link2='Women' classLink1='part-men' classLink2='part-women'/>
                <CardsRow name='New Arrivals' flag='arrival'/>
                <CardsRow name='Best sellers' fleg='bestseller'/>
                <Section2 link1='Goods' link2='Journal' classLink1='part-coods' classLink2='part-journal'/>
                <Section3/>
                <Section4/>
                <Footer />
            </div>
        )
    }
       
}

export default Main;