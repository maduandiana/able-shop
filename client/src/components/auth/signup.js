import React from 'react';
import Header from '../header/index.js'
import Footer from '../footer/index.js'
import {Link} from 'react-router-dom'
import { Form, Input, Button} from 'antd';
import {signUp} from '../../actions/authActions'
import connect from "react-redux/es/connect/connect";

function SignUp(props){
    function sendSignUp (values){
        props.signUp(values,props.history)
    }

    return(
        <section className='auth-section'>
            <Header/>
            <div className='auth-div'>
                <h3>Create a P&Co account</h3>
                <p>Already have an account? <Link to='/login'>Sign in</Link></p>
                <Form 
                name="basic"
                initialValues={{ remember: true }} 
                className='auth-form'
                onFinish = {sendSignUp}
                >
                <Form.Item
                    name="username"
                    rules={[{ required: true, message: 'Please input your username!' }]}
                >
                    <Input placeholder='Username'/>
                </Form.Item>
                <Form.Item
                    name="email"
                    rules={[{ required: true, message: 'Please input your email!' }]}
                >
                    <Input placeholder='Email'/>
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password placeholder='Password'/>
                </Form.Item>
                <Form.Item
                    name="password"
                    rules={[{ required: true, message: 'Please input your password!' }]}
                >
                    <Input.Password placeholder='Confirm Password'/>
                </Form.Item>

                <Form.Item>
                    <Button type="primary" htmlType="submit" className='submit-btn'>
                    Submit
                    </Button>
                </Form.Item>
                </Form>
                <p>By providing your email address, you agree to our Privacy Policy and Terms & Conditions</p>
            </div>
            <Footer />
        </section>
    )
}

const mapStateToProps = (state) => ({
    
});

export default connect(mapStateToProps, {signUp})(SignUp);
