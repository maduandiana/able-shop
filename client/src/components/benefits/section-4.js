import React, {Component} from 'react'
class Section4 extends Component{
    render(){
        return(
            <div className='section-4'>
                <img src='/images/card.png' alt='section-4' />
                <p>We know that nothing ever worth holding onto was built overnight. That’s why we trust in the process. We take our time and do it right. We’re proud that our goods go through some seriously hard craft. We explore the road less travelled and go where others don’t. Because life shouldn’t be boring.</p>
            </div>
        )
    }
}

export default Section4;