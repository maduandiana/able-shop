import React, {Component} from 'react'
class Benefits extends Component{
    render(){
        return(
            <div className='benefits'>
                <div className='benefits-item'>
                    <img src='https://cdn.shopify.com/s/files/1/0235/2617/files/sale-icon_180x.png?v=1595516820' alt='icon' />
                    <p>15% Off First Order</p>
                    <p>Subscribe to our mailing list <br/>
                    for 15% off your first order.</p>
                </div>
                <div className='benefits-item'>
                    <img src='https://cdn.shopify.com/s/files/1/0235/2617/files/fast_db92c728-18b3-4adb-8edb-89df43c4d565_180x.png?v=1596095767' alt='icon' />
                    <p>Extended 60 Day Returns</p>
                    <p>Shop with certainty with an <br/>
                    extended 60 day returns policy.</p>
                </div>
                <div className='benefits-item'>
                    <img src='https://cdn.shopify.com/s/files/1/0235/2617/files/globe_1293d8fc-d66a-4abc-9eab-9f5e624473de_180x.png?v=1595516869' alt='icon' />
                    <p>Worldwide Shipping</p>
                    <p>Free worldwide shipping on <br/>
                    all orders over £50.</p>
                </div>
            </div>
        )
    }
}

export default Benefits;