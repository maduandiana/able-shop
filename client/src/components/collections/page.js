import React, {Component} from 'react'
import {Link} from 'react-router-dom'
class Section2 extends Component{
    componentDidMount(){
        window.scrollTo(0,0)
    }
    render(){
        return(
            <div className='section-2'>
                <div className={this.props.classLink1}>
                    <Link to='/collections/men'>
                        <h1 className='shop-h1'>{this.props.link1}</h1>
                    </Link>
                </div>
                <div className={this.props.classLink2}>
                    <Link to='/collections/women'>
                        <h1 className='shop-h1'>{this.props.link2}</h1>
                    </Link>
                </div>
            </div>
        )
    }
}

export default Section2;