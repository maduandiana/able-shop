import React, {Component} from 'react'
import Header from '../header/index'
import Footer from '../footer/index'
import {getProducts} from '../../actions/productActions'
import connect from "react-redux/es/connect/connect";
import Card from '../card/index'
class Page extends Component{
    componentDidMount(){
        this.props.getProducts()
        window.scrollTo(0,0)
    }
    render(){
        // eslint-disable-next-line
        let cardList
        return(
            <div>
                <Header/>
                <h1 className='title'>{this.props.match.params.type}s</h1>
                <div className='row-collections'>
                {
                        this.props.productReducer.products && this.props.productReducer.products.data ?
                        cardList = this.props.productReducer.products.data.map((item) =>(
                            item.type === this.props.match.params.type ?
                            <Card key={item._id} id={item._id} name={item.name} description={item.description} details={item.details} price={item.price} type={item.type} image={item.image}
                                        image2={item.image2} image3={item.image3}/> : 
                                        null
                        )):null 
                }
                </div>
                <Footer/>
            </div>
        )
    }
}

const mapStateToProps = (state) => ({
    productReducer: state.productReducer
});

export default connect(mapStateToProps, {getProducts})(Page);
