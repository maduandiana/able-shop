import { UserOutlined,InfoCircleOutlined,WechatOutlined,StarOutlined,MailOutlined,InstagramOutlined,FacebookOutlined,TwitterOutlined,YoutubeOutlined,ArrowRightOutlined } from '@ant-design/icons'
import React, {Component} from 'react'
import {Link} from 'react-router-dom'
class Footer extends Component{
    render(){
        return(
            <div className='footer'>
                <div className='footer-1'>
                    <h3>Info</h3>
                    <div className='links'>
                        <Link to='/'><UserOutlined className='links-icon'/> Sign in</Link>
                        <Link to='/'><InfoCircleOutlined className='links-icon' /> Help center</Link>
                        <Link to='/'><WechatOutlined className='links-icon'/> Live chat</Link>
                        <Link to='/'><StarOutlined className='links-icon'/>Careers</Link>
                        <Link to='/'><MailOutlined className='links-icon'/> Contacts</Link>
                    </div>
                    <div className='social-links'>
                        <a href='www.instagram.com'><InstagramOutlined className='social-icons'/></a>
                        <a href='www.instagram.com'><TwitterOutlined className='social-icons'/></a>
                        <a href='www.instagram.com'><YoutubeOutlined className='social-icons'/></a>
                        <a href='www.instagram.com'><FacebookOutlined className='social-icons'/></a>
                    </div>
                    <div className='payments'>
                        <svg aria-hidden="true" focusable="false" data-prefix="fab" data-icon="apple-pay" class="svg-inline--fa fa-apple-pay fa-w-20" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M116.9 158.5c-7.5 8.9-19.5 15.9-31.5 14.9-1.5-12 4.4-24.8 11.3-32.6 7.5-9.1 20.6-15.6 31.3-16.1 1.2 12.4-3.7 24.7-11.1 33.8m10.9 17.2c-17.4-1-32.3 9.9-40.5 9.9-8.4 0-21-9.4-34.8-9.1-17.9.3-34.5 10.4-43.6 26.5-18.8 32.3-4.9 80 13.3 106.3 8.9 13 19.5 27.3 33.5 26.8 13.3-.5 18.5-8.6 34.5-8.6 16.1 0 20.8 8.6 34.8 8.4 14.5-.3 23.6-13 32.5-26 10.1-14.8 14.3-29.1 14.5-29.9-.3-.3-28-10.9-28.3-42.9-.3-26.8 21.9-39.5 22.9-40.3-12.5-18.6-32-20.6-38.8-21.1m100.4-36.2v194.9h30.3v-66.6h41.9c38.3 0 65.1-26.3 65.1-64.3s-26.4-64-64.1-64h-73.2zm30.3 25.5h34.9c26.3 0 41.3 14 41.3 38.6s-15 38.8-41.4 38.8h-34.8V165zm162.2 170.9c19 0 36.6-9.6 44.6-24.9h.6v23.4h28v-97c0-28.1-22.5-46.3-57.1-46.3-32.1 0-55.9 18.4-56.8 43.6h27.3c2.3-12 13.4-19.9 28.6-19.9 18.5 0 28.9 8.6 28.9 24.5v10.8l-37.8 2.3c-35.1 2.1-54.1 16.5-54.1 41.5.1 25.2 19.7 42 47.8 42zm8.2-23.1c-16.1 0-26.4-7.8-26.4-19.6 0-12.3 9.9-19.4 28.8-20.5l33.6-2.1v11c0 18.2-15.5 31.2-36 31.2zm102.5 74.6c29.5 0 43.4-11.3 55.5-45.4L640 193h-30.8l-35.6 115.1h-.6L537.4 193h-31.6L557 334.9l-2.8 8.6c-4.6 14.6-12.1 20.3-25.5 20.3-2.4 0-7-.3-8.9-.5v23.4c1.8.4 9.3.7 11.6.7z"></path></svg>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512.04 137.3" fill="currentColor"><path d="M61.64,0h-40a5.28,5.28,0,0,0-5.2,4.7L0,106.8a3.44,3.44,0,0,0,3.2,4h19c2.7,0,5.2-2.9,5.5-5.7l4.5-26.6c1-7.2,13.2-4.7,18-4.7,28.6,0,46.1-17,46.1-45.8C96.34,7,80.14,0,61.64,0ZM66,37.2c-2,12.7-10.5,14.2-21.5,14.2l-8.2.3L40.64,25a3.13,3.13,0,0,1,3.2-2.7h4.7C59.84,22.3,68.34,23.8,66,37.2Z"></path><polygon points="380.34 28 380.34 28 380.34 28 380.34 28"></polygon><path d="M345.64,0h-39.7a5.51,5.51,0,0,0-5.5,4.7l-16.2,102c-.2,2,1.3,4,3.2,4h20.5a4.29,4.29,0,0,0,4-3.2l4.5-29c1-7.2,13.2-4.7,18-4.7,28.4,0,45.9-17,45.9-45.8C380.34,7,364.14,0,345.64,0Zm-17,51.4-8,.3,4.2-26.7a2.94,2.94,0,0,1,3.2-2.7h4.5c8.5,0,18,.5,18.1,11.1C350.64,48.4,341.64,51.4,328.64,51.4Z"></path><path d="M221,134.7l63.7-92.6a2.05,2.05,0,0,0,.5-1.7,3.57,3.57,0,0,0-3.2-3.5h-19.2a5.66,5.66,0,0,0-4.5,2.5l-26.5,39-11-37.5a6.07,6.07,0,0,0-5.5-4h-18.7a3.57,3.57,0,0,0-3.2,3.5c0,1.2,19.5,56.8,21.2,62.1-2.7,3.8-20.5,28.6-20.5,31.6a3.22,3.22,0,0,0,3.2,3.2h19.2a5.89,5.89,0,0,0,4.5-2.6Z"></path><path d="M512,3.5a3.36,3.36,0,0,0-3.2-3.5h-18.5a3.32,3.32,0,0,0-3.2,2.7l-16.2,104-.3.5a3.54,3.54,0,0,0,3.5,3.5h16.5c2.5,0,5-2.9,5.2-5.7L512,3.8V3.5Z"></path><path d="M464.54,36.8h-19c-3.8,0-4,5.5-4.3,8.2-5.5-8.5-14-10-23.7-10-24.5,0-43.2,21.5-43.2,45.2,0,19.5,12.2,32.2,31.7,32.2,9.3,0,20.5-4.9,26.5-11.9a44.44,44.44,0,0,0-1,6.2c0,2.3,1,4,3.2,4H452c2.7,0,5-2.9,5.5-5.7l10.2-64.3A3.4,3.4,0,0,0,464.54,36.8Zm-48,55.5c-9.2,0-16.2-5.3-16.2-15a21.62,21.62,0,0,1,21.7-22c9.3,0,16.3,5.7,16.2,15.5A21.32,21.32,0,0,1,416.54,92.3Z"></path><path d="M180.54,36.8h-19c-3.8,0-4,5.5-4.2,8.2-5.8-8.5-14.2-10-23.7-10-24.5,0-43.2,21.5-43.2,45.2,0,19.5,12.2,32.2,31.7,32.2,9,0,20.2-4.9,26.5-11.9a26.72,26.72,0,0,0-1,6.2c0,2.3,1,4,3.2,4H168c2.7,0,5-2.9,5.5-5.7l10.2-64.3A3.4,3.4,0,0,0,180.54,36.8Zm-48.2,55.5c-9.2,0-16-5.2-16-15a21.68,21.68,0,0,1,21.7-22c9.3,0,16.3,5.7,16.3,15.5C154.34,83,144.64,92.3,132.34,92.3Z"></path></svg>
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 34 19" fill="currentColor"><circle cx="24.5" cy="9.5" r="9.5"></circle><path d="M12,9.5a12.44,12.44,0,0,1,2.8-7.88,9.5,9.5,0,1,0,0,15.77A12.44,12.44,0,0,1,12,9.5Z"></path></svg>
                    </div>
                    <div className='links terms'>
                        <Link to='/'>Copyright ©2020 Terms & Conditions</Link>
                        <Link to='/'>Privacy & Cookies</Link>
                        <Link to='/'>Site by Shopify Plus Agency - Digital Cake</Link>
                    </div>
                    <p className='p-link'>Clark & Timms Ltd. <br></br>Company no. 08442586</p>
                </div>
                <div className='footer-2'>
                    <h3>Keep in touch</h3>
                    <p>I'm interested in:</p>
                    <label class="container">Women's
                        <input type="checkbox" checked="checked" />
                        <span class="checkmark"></span>
                    </label>
                    <label class="container">Men's
                        <input type="checkbox" />
                        <span class="checkmark"></span>
                    </label>
                    <input type='email' className='email-input' placeholder='Enter your email...'></input>
                    <ArrowRightOutlined className='email-icon'/>
                </div>
            </div>
        )
    }
}

export default Footer;