import React from 'react'
import {HeartOutlined} from '@ant-design/icons'
import {Link} from 'react-router-dom'
function Card(props){
    return(
        <section className='card'>
            <Link to={'details/'+ props.id}>
                <div>
                    <span className='span-new'>NEW</span>
                    <img className='card-img' src={props.image} alt='card'></img>
                    <HeartOutlined className='heart-icon'/>
                </div>
                <p>{props.type}</p>
                <h3>{props.name}</h3>
                <p>{props.price}</p>
            </Link>
            
        </section>   
    )
}
export default Card;