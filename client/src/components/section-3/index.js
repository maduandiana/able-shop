import React, {Component} from 'react'
//import {Link} from 'react-router-dom'
class Section3 extends Component{
    render(){
        return(
            <div className='section-3'>
                <div className='section3-img'>
                   <img src='/images/pants.png' alt='pants' />
                </div>
                <div className='section3-text'>
                   <h1>Service <br></br> Fatigue Pants</h1>
                   <h4>Creative Uniform</h4>
                   <p>Our best fitting pair of bottoms to date, our <b>Service Fatigue Pants</b> are the perfect alternative to denim.</p>
                   <button>Explore the fatiques</button>
                </div>
            </div>
        )
    }
}

export default Section3;