import {GET_BASKET, GET_ERRORS} from './types'
import axios from 'axios/index'

export const getBasket = () => dispatch => {
    axios.get("http://localhost:3000/basket")
    .then(response => {
        return dispatch ({
            type:GET_BASKET,
            payload:response.data
        })
    }).catch(err =>{
        return dispatch({
            type:GET_BASKET,
            payload:err.response.data
        })
    })
}

export const addToBasket = (data,) => dispatch => {
    axios.post("http://localhost:3000/basket/add",data)
    .then(response => {
        dispatch(getBasket())
        return dispatch ({
            type:GET_BASKET,
            payload:response.data
        })
    }).catch(err =>{
        return dispatch({
            type:GET_ERRORS,
            payload:err.response.data
        })
    })
}

export const deleteBasket = (id) => dispatch => {
    axios.delete("http://localhost:3000/basket/" + id)
    .then(response => {
        dispatch(getBasket())
        
    }).catch(err =>{
        dispatch(getBasket())
        console.log(err)
    })
}
