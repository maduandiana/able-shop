import {GET_PRODUCTS,GET_ERRORS,GET_PRODUCT} from './types'
import axios from 'axios/index'

export const getProducts = () => dispatch => {
    axios.get("http://localhost:3000/product")
    .then(response => {
        return dispatch ({
            type:GET_PRODUCTS,
            payload:response.data
        })
    }).catch(err =>{
        return dispatch({
            type:GET_ERRORS,
            payload:err.response.data
        })
    })
}

export const getProductById = (id) => dispatch => {
    axios.get("http://localhost:3000/product/" + id)
    .then(response => {
        return dispatch ({
            type:GET_PRODUCT,
            payload:response.data
        })
    }).catch(err =>{
        return dispatch({
            type:GET_ERRORS,
            payload:err.response.data
        })
    })
}

export const deleteProductById = (id) => dispatch => {
    axios.delete("http://localhost:3000/product/" + id)
    .then(response => {
        dispatch(getProducts())
    }).catch(err =>{
        dispatch(getProducts())
        return dispatch({
            type:GET_ERRORS,
            payload:err.response.data
        })
    })
}

export const saveProduct = (data,closeModal,onReset) => dispatch => {
    axios.post("http://localhost:3000/product/add",data)
    .then(response => {
        closeModal()
        onReset()
        dispatch(getProducts())
        return dispatch ({
            type:GET_PRODUCTS,
            payload:response.data
        })
    }).catch(err =>{
        return dispatch({
            type:GET_ERRORS,
            payload:err.response.data
        })
    })
}