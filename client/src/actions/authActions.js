import {GET_ERRORS,GET_PROFILE,SET_CURRENT_USER} from './types'
import axios from 'axios/index'
import setAuthToken from '../utils/setToken'
import jwt_decode from 'jwt-decode'
export const signUp = (userdata,history) => dispatch => {
    axios.post("http://localhost:3000/auth/signup",userdata)
    .then(response => {
        const {token} = response.data
        localStorage.setItem('jwtToken',token);
        setAuthToken(token)
        const decoded = jwt_decode(token)
        dispatch(setCurrentUser(decoded))
        history.push('/')
        return dispatch ({
            type:GET_ERRORS,
            payload:false
        })
    }).catch(err =>{
        return dispatch({
            type:GET_ERRORS,
            payload:err.response.data
        })
    })
}


export const login = (userdata,history) => dispatch => {
    axios.post("http://localhost:3000/auth/login",userdata)
    .then(response => {
        const {token} = response.data
        localStorage.setItem('jwtToken',token);
        setAuthToken(token)
        const decoded = jwt_decode(token)
        dispatch(setCurrentUser(decoded))
        history.push('/')
        return dispatch ({
            type:GET_ERRORS,
            payload:false
        })
    }).catch(err =>{
        return dispatch({
            type:GET_ERRORS,
            payload:err.response.data
        })
    })
}

export const setCurrentUser = (decoded) => {
    return {
        type: SET_CURRENT_USER,
        payload: decoded
    }
}

export const logOut = () => dispatch => {
    localStorage.removeItem('jwtToken');
    localStorage.removeItem('role');
    setAuthToken(false)
    dispatch(setCurrentUser({}))
}

export const getProfile = () => dispatch => {
    axios.get("http://localhost:3000/auth/profile")
    .then(response => {
        const {role} = response.data.data
        localStorage.setItem('role',role);
        return dispatch ({
            type:GET_PROFILE,
            payload:response.data
        })
    }).catch(err =>{
        return dispatch({
            type:GET_ERRORS,
            payload:err.response.data
        })
    })
}